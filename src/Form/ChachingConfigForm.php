<?php

namespace Drupal\chaching\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a Cha-ching config form.
 */
class ChachingConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chaching_config_form';
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Editable config names.
   */
  protected function getEditableConfigNames() {
    return ['chaching.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return mixed[]
   *   Form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['receiver_email'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Receiver email addresses'),
      '#description'   => $this->t('A list of the email addresses associated with your PayPal account which may receive a payment (one per line).'),
      '#config_target' => new ConfigTarget(
        'chaching.settings',
        'receiver_email',
        fn($value) => implode("\n", $value),
        fn($value) => array_map('trim', preg_split('/\R/', $value, -1, PREG_SPLIT_NO_EMPTY) ?: []),
      ),
    ];
    return parent::buildForm($form, $form_state);
  }

}
