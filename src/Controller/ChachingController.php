<?php

namespace Drupal\chaching\Controller;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Link;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for Cha-ching pages.
 */
class ChachingController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('database'),
      $container->get('extension.path.resolver'),
    );
  }

  /**
   * {@inheritdoc}
   */
  final public function __construct(
    protected Connection $database,
    protected ExtensionPathResolver $extensionPathResolver,
  ) {
  }

  /**
   * IPN callback.
   *
   * @return string[]
   *   Renderable array.
   */
  public function ipn(Request $request): array {
    $ipn = $request->request->all();
    $validated = $this->validate($ipn);
    if (!$validated) {
      $this->getLogger('chaching')->error('IPN not verified: %ipn', ['%ipn' => var_export($ipn, TRUE)]);
      throw new \Exception('IPN incoming NOT VERIFIED');
    }
    $receiver_emails = $this->config('chaching.settings')->get('receiver_email');
    if (!$receiver_emails || !is_array($receiver_emails)) {
      throw new \Exception('PayPal receiver email not configured.');
    }
    if (array_search($ipn['receiver_email'], $receiver_emails) === FALSE) {
      throw new \Exception('PayPal IPN contains invalid receiver email address.');
    }
    $this->moduleHandler()->loadInclude('chaching', 'install');
    $schema = chaching_schema()['chaching_paypal_ipns'];
    foreach ($ipn as $key => $value) {
      if (!isset($schema['fields'][$key])) {
        $this->getLogger('chaching')->error('IPN unknown field ignored: %key => %value', [
          '%key' => $key,
          '%value' => $value,
        ]);
        unset($ipn[$key]);
      }
    }
    $ipn['timestamp'] = time();
    $ipn['ipn'] = Yaml::encode($request->request->all());
    if (!empty($ipn['payment_date'])) {
      $ipn['payment_timestamp'] = strtotime($ipn['payment_date']);
    }
    $ipn['verification_result'] = $validated;
    unset($ipn['processed']);
    $this->database->insert('chaching_paypal_ipns')
      ->fields($ipn)
      ->execute();
    Cache::invalidateTags(['chaching']);
    return [
      '#markup' => 'IPN: Only PayPal will ever see this page - humans go away!',
    ];
  }

  /**
   * Check with PayPal that this is a real IPN.
   *
   * Without this check, IPNs may be spoofed.
   *
   * @param string[] $post
   *   The key-value pairs from an incoming IPN (presumably from $_POST).
   *
   * @return bool
   *   TRUE if PayPal successfully validated the request.  This will not happen
   *   if there was an error communicating with the service, the IPN did not
   *   originate from PayPal, or it was tampered with in any way.
   */
  public function validate(array $post) {
    // Build the IPN validation request from the $post fields.
    $post['cmd'] = '_notify-validate';
    $req = http_build_query($post);
    $hostname = empty($post['test_ipn']) ? 'www.paypal.com' : 'www.sandbox.paypal.com';
    $ch = curl_init("https://$hostname/cgi-bin/webscr");
    if (!$ch) {
      throw new \Exception('Unknown cURL init error.');
    }
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Connection: Close']);
    $res = curl_exec($ch);
    curl_close($ch);
    if (is_bool($res)) {
      return FALSE;
    }
    return trim($res) === 'VERIFIED';
  }

  /**
   * Donations API callback.
   */
  public function donations(Request $request, string $type = 'total', string $period = 'mtd', string $format = 'json', string $filter = ''): CacheableResponseInterface {
    $timezone = new \DateTimeZone('America/Los_Angeles');
    $date = new \DateTime('now', $timezone);
    $output = new \stdClass();
    $output->end = $date->format('r');
    switch (strtolower($period)) {
      case 'mtd':
        $date->setDate((int) $date->format('Y'), (int) $date->format('n'), 1)->setTime(0, 0);
        break;

      case '1m':
        $date->modify('-1 month');
        break;

      case 'ytd':
        $date->setDate((int) $date->format('Y'), 1, 1)->setTime(0, 0);
        break;

      case '1y':
        $date->modify('-1 year');
        break;

      case 'all':
        $date->setDate(2006, 12, 17)->setTime(12, 0);
        break;

      default:
        throw new NotFoundHttpException();
    }
    $output->start = $date->format('r');
    switch (strtolower($type)) {
      case 'total':
        $statement = $this->database->query('SELECT SUM(mc_gross) AS total_gross, SUM(mc_gross - mc_fee) AS total_net, COUNT(*) AS count FROM {chaching_paypal_ipns} WHERE payment_status = :status AND timestamp >= :timestamp', [
          ':status' => 'Completed',
          ':timestamp' => $date->getTimestamp(),
        ]);
        if (!$statement instanceof StatementInterface) {
          throw new \UnexpectedValueException('No database statement.');
        }
        $total = $statement->fetchObject();
        if (!is_object($total) || !isset($total->total_gross, $total->total_net, $total->count)) {
          throw new \UnexpectedValueException('No data returned.');
        }
        $output->total_gross = (float) $total->total_gross;
        $output->total_net = (float) $total->total_net;
        $output->count = (int) $total->count;
        break;

      case 'list':
        $output->donations = [];
        $payment_date = new \DateTime('now', $timezone);
        $result = $this->database->query('SELECT timestamp, mc_gross, mc_fee, mc_gross - mc_fee AS payment_net, id, txn_type FROM {chaching_paypal_ipns} WHERE payment_status = :status AND timestamp >= :timestamp', [
          ':status' => 'Completed',
          ':timestamp' => $date->getTimestamp(),
        ]);
        if (!$result instanceof StatementInterface) {
          throw new \UnexpectedValueException('No database result.');
        }
        foreach ($result as $donation) {
          $donation->timestamp = (int) $donation->timestamp;
          $donation->id = (int) $donation->id;
          $payment_date->setTimestamp($donation->timestamp);
          $donation->payment_date = $payment_date->format('r');
          $donation->payment_fee = $donation->mc_fee = (float) $donation->mc_fee;
          $donation->payment_gross = $donation->mc_gross = (float) $donation->mc_gross;
          $donation->payment_net = (float) $donation->payment_net;
          $donation->recurring = ($donation->txn_type === 'recurring_payment' || $donation->txn_type === 'subscr_payment');
          $output->donations[] = $donation;
        }
        $output->count = count($output->donations);
        break;

      default:
        throw new NotFoundHttpException();
    }
    switch (strtolower($format)) {
      case 'json':
        if (empty($request->query->get('callback')) || preg_match('/\W/', (string) $request->query->get('callback'))) {
          $response = new CacheableJsonResponse($output, 200, ['Access-Control-Allow-Origin' => '*']);
        }
        else {
          $response = new CacheableResponse($request->query->get('callback') . '(' . json_encode($output) . ');', 200, ['Content-Type' => 'application/javascript; charset=utf-8']);
        }
        $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
          '#cache' => [
            'contexts' => ['url.query_args:callback'],
            'tags' => ['chaching'],
          ],
        ]));
        return $response;

      case 'rss':
        $output->filter = $filter;
        return $this->rss($output, $request);

      default:
        throw new NotFoundHttpException();
    }
  }

  /**
   * Graph page callback.
   *
   * @return mixed[]
   *   Renderable array.
   */
  public function graph(string $period = '1m', string $filter = ''): array {
    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'id' => 'chaching-plot',
      ],
      '#attached' => [
        'drupalSettings' => [
          'chaching' => ['period' => $period, 'filter' => $filter],
        ],
        'feed' => [
          [
            Url::fromUri("internal:/v1/donations/list/$period/rss/$filter")->toString(),
            'Noisebridge donations RSS',
          ],
        ],
        'library' => [
          'chaching/graph',
        ],
      ],
    ];
  }

  /**
   * Docs page callback.
   *
   * @return mixed[]
   *   Renderable array.
   */
  public function docs(): array {
    $parameters = [
      ['type' => 'total', 'period' => '1m', 'format' => 'json'],
      ['type' => 'total', 'period' => '1y', 'format' => 'json'],
      ['type' => 'list', 'period' => '1m', 'format' => 'json'],
      ['type' => 'list', 'period' => '1y', 'format' => 'json'],
    ];
    $descriptions = [
      'Total donations to Noisebridge in past month.',
      'Total donations to Noisebridge in past year.',
      'List of donations to Noisebridge in past month.',
      'List of donations to Noisebridge in past year.',
    ];
    foreach ($parameters as $key => $item) {
      $url = Url::fromRoute('chaching.donations', $item, ['absolute' => TRUE]);
      $json[] = [
        Link::fromTextAndUrl($url->toString(), $url)->toRenderable(),
        ['#markup' => '<br />' . $descriptions[$key]],
      ];
    }
    $output[] = [
      '#theme' => 'item_list',
      '#items' => $json,
      '#title' => 'JSON format',
    ];
    $url = Url::fromRoute('chaching.donations', [
      'type' => 'list',
      'period' => '1m',
      'format' => 'rss',
    ], ['absolute' => TRUE]);
    $rss[] = [
      Link::fromTextAndUrl($url->toString(), $url)->toRenderable(),
      ['#markup' => '<br />' . 'List of donations to Noisebridge in past month.'],
    ];
    $output[] = [
      '#theme' => 'item_list',
      '#items' => $rss,
      '#title' => 'RSS format',
    ];
    $output[] = [
      '#markup' => '<blockquote><pre>' . file_get_contents($this->extensionPathResolver->getPath('module', 'chaching') . '/sample-code.js') . '</pre></blockquote>',
    ];
    return $output;
  }

  /**
   * Outputs data in RSS format.
   */
  public function rss(\stdClass $var, Request $request): CacheableResponse {
    global $base_url;
    $language = $this->languageManager()->getCurrentLanguage();
    $items = '';
    if (isset($var->donations)) {
      foreach (array_reverse($var->donations) as $item) {
        $description = '$' . $item->mc_gross . ' donation received';
        $extra = [
          'pubDate' => gmdate('r', $item->timestamp),
          [
            'key' => 'guid',
            'value' => $base_url . '/ipn/' . $item->id,
            'attributes' => ['isPermaLink' => 'false'],
          ],
        ];
        $items .= $this->formatRssItem('Cha-ching! ' . $description, $base_url, $description . '.', $extra);
      }
    }
    $title = 'Noisebridge donations';
    $description = 'Latest donations to Noisebridge.';
    $output = '<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="' . $base_url . '" xmlns:atom="http://www.w3.org/2005/Atom">
';
    $output .= $this->formatRssChannel($title, $base_url, $description, $items, $language->getId(), [
      [
        'key' => 'atom:link',
        'attributes' => [
          'href' => $base_url . $request->getRequestUri(),
          'rel' => 'self',
          'type' => 'application/rss+xml',
        ],
      ],
    ]) . '</rss>';
    $response = new CacheableResponse($output, 200, [
      'Access-Control-Allow-Origin' => '*',
      'Content-Type' => 'application/rss+xml; charset=utf-8',
    ]);
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray(['#cache' => ['tags' => ['chaching']]]));
    return $response;
  }

  /**
   * Formats RSS channel.
   *
   * @param string $title
   *   Feed title.
   * @param string $link
   *   Feed link.
   * @param string $description
   *   Feed description.
   * @param string $items
   *   Feed items.
   * @param string $langcode
   *   Feed language.
   * @param mixed[] $args
   *   Feed elements.
   */
  public function formatRssChannel(string $title, string $link, string $description, string $items, ?string $langcode = NULL, array $args = []): string {
    $language_content = $this->languageManager()->getCurrentLanguage();
    $langcode = $langcode ? $langcode : $language_content->getId();
    $output = "<channel>\n";
    $output .= ' <title>' . Html::escape($title) . "</title>\n";
    $output .= ' <link>' . Html::escape($link) . "</link>\n";

    // The RSS 2.0 "spec" doesn't indicate HTML can be used in the description.
    // We strip all HTML tags, but need to prevent double encoding from properly
    // escaped source data (such as &amp becoming &amp;amp;).
    $output .= ' <description>' . Html::escape(Html::decodeEntities(strip_tags($description))) . "</description>\n";
    $output .= ' <language>' . Html::escape($langcode) . "</language>\n";
    $output .= $this->formatXmlElements($args);
    $output .= $items;
    $output .= "</channel>\n";
    return $output;
  }

  /**
   * Formats RSS item.
   *
   * @param string $title
   *   Item title.
   * @param string $link
   *   Item link.
   * @param string $description
   *   Item description.
   * @param mixed[] $args
   *   Item elements.
   */
  public function formatRssItem(string $title, string $link, string $description, array $args = []): string {
    $output = "<item>\n";
    $output .= ' <title>' . Html::escape($title) . "</title>\n";
    $output .= ' <link>' . Html::escape($link) . "</link>\n";
    $output .= ' <description>' . Html::escape($description) . "</description>\n";
    $output .= $this->formatXmlElements($args);
    $output .= "</item>\n";
    return $output;
  }

  /**
   * Formats XML elements.
   *
   * @param mixed[] $array
   *   Array of elements.
   */
  public function formatXmlElements(array $array): string {
    $output = '';
    foreach ($array as $key => $value) {
      if (is_numeric($key)) {
        if (is_array($value) && $value['key']) {
          $output .= ' <' . $value['key'];
          if (isset($value['attributes']) && is_array($value['attributes'])) {
            $output .= new Attribute($value['attributes']);
          }
          if (isset($value['value']) && $value['value'] != '') {
            $output .= '>' . (is_array($value['value']) ? $this->formatXmlElements($value['value']) : (!empty($value['encoded']) ? $value['value'] : Html::escape($value['value']))) . '</' . $value['key'] . ">\n";
          }
          else {
            $output .= " />\n";
          }
        }
      }
      else {
        $output .= ' <' . $key . '>' . (is_array($value) ? $this->formatXmlElements($value) : (is_string($value) ? Html::escape($value) : '')) . "</{$key}>\n";
      }
    }
    return $output;
  }

}
