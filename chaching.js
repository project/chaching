(($, Drupal) => {
  Drupal.behaviors.chaching = {
    attach(context, settings) {
      $('#chaching-plot:not(.chaching-processed)', context).each(() => {
        $.jqplot.config.enablePlugins = true;
        $.ajax({
          url: `${settings.path.baseUrl}v1/donations/list/${settings.chaching.period}/json/${settings.chaching.filter}`,
          data: {},
          dataType: 'json',
          cache: true,
          success(data) {
            const points = [[data.start, 0]];
            let total = 0;
            $.each(data.donations, (i, donation) => {
              total += donation.mc_gross;
              points.push([donation.payment_date, total]);
            });
            points.push([data.end, total]);
            $.jqplot('chaching-plot', [points], {
              axes: {
                xaxis: {
                  renderer: $.jqplot.DateAxisRenderer,
                  tickOptions: { formatString: '%#m/%#d/%y' },
                  autoscale: true,
                },
                yaxis: {
                  tickOptions: { formatString: '$%d' },
                  autoscale: true,
                },
              },
              series: [{ fill: true, fillToZero: true, showMarker: false }],
            });
          },
        });
        $(this).addClass('chaching-processed');
      });
    },
  };
})(jQuery, Drupal);
