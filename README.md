https://cha-ching.noisebridge.net/
==================================

Cha-ching is a donation register.  It records instant payment notification (IPN)
data received from PayPal, and makes the metadata (payment amount and date)
available via JSON and RSS feeds for display and graphing purposes.

Cha-ching was originally created for Noisebridge, an educational hackerspace in
San Francisco, California: https://www.noisebridge.net/

Additional features will be added in the future.  You can support development by
contributing bug reports and feature requests at
https://www.drupal.org/project/issues/chaching or by sponsoring:
https://github.com/sponsors/mfb
