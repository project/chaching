<?php

namespace Drupal\Tests\chaching\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Cha-ching module.
 *
 * @group chaching
 */
class ChachingTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['chaching'];

  /**
   * Tests Cha-ching module.
   */
  public function testChaching(): void {

  }

}
