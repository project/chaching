/**
 * Sample javascript code for drawing a donation graph.
 *
 * @requires
 * - jquery.js
 * - jquery.jqplot.js
 * - jqplot.dateAxisRenderer.js
 */

(($) => {
  $.jqplot.config.enablePlugins = true;
  $.ajax({
    url: 'https://cha-ching.noisebridge.net/v1/donations/list/1m/json',
    data: {},
    dataType: 'json',
    cache: true,
    success(data) {
      const points = [[data.start, 0]];
      let total = 0;
      $.each(data.donations, (i, donation) => {
        total += donation.mc_gross;
        points.push([donation.payment_date, total]);
      });
      points.push([data.end, total]);
      $.jqplot('my-graph', [points], {
        axes: {
          xaxis: {
            renderer: $.jqplot.DateAxisRenderer,
            tickOptions: { formatString: '%#m/%#d/%y' },
            autoscale: true,
          },
          yaxis: { tickOptions: { formatString: '$%d' }, autoscale: true },
        },
        series: [{ fill: true, fillToZero: true, showMarker: false }],
      });
    },
  });
})(jQuery);
